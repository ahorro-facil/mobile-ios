package com.MobileiOS.mobileiOS;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MobileIOsApplication {

	public static void main(String[] args) {
		SpringApplication.run(MobileIOsApplication.class, args);
	}

}
